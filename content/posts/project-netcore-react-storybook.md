---
title: "Project Netcore React Storybook"
date: 2021-07-11T16:02:06-04:00
draft: false
---

## Crear un proyecto de .NetCore

```
dotnet new react -o [name]
```

### Para agregar storybook

Se debe entrar a la carpeta Client dentro del proyecto y correr el siguiente comando:

```
npx sb init
```
