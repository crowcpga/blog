---
title: "Hugo"
date: 2021-07-11T15:51:40-04:00
draft: false
description: Hugo y comandos iniciales
---

> En este post veremos como se instala, se crea un sitio, se crea un post y luego como podemos tener un servidor.
> También veremos como instalar un tema de hugo para aplicarlo a nuestro nuevo sitio.

## Como instalar Hugo CMS

```
brew install hugo
```

## Como crear un sitio con hugo CMS

```
hugo new site [name] -f yml
```

## Como crear un post con hugo CMS

```
hugo new posts/[name].md
```

## Como correr hugo en local

```
hugo serve --watch
```

## Como instalar un tema de hugo

```
git clone https://github.com/adityatelange/hugo-PaperMod themes/PaperMod --depth=1

echo "theme=PaperMod" >> config.yml
```
